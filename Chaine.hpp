#ifndef CHAINE_HPP
#define CHAINE_HPP
#define CAPACITE_DEFAUT 16

#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <string.h>

class Chaine
{
private:
	char *str ;
	int capacity ;
	int strlen ;
public:
	Chaine() ;
  Chaine(const Chaine&) ;
  ~Chaine() ;

  Chaine& operator=(const Chaine&) ;
  friend std::ostream& operator<<(std::ostream &out, const Chaine& c)
  {
    if(c.str) {
        out << c.str ;
    }

    return out ;
  }

  void setStr(const char* chaine) ;
};

#endif // CHAINE_HPP
