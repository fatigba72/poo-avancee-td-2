#ifndef LIST_HPP_INCLUDED
#define LIST_HPP_INCLUDED

#include <iostream>

// On déclare qu'une classe template existe pour la liste
template <class E> class List ;

template <class T>
class Element
{
	private:
    Element<T> *next;
    T data ;
    friend class List<T> ;
	public:
    Element() ;
    Element(const Element<T>& e)  ;
    ~Element() ;
    Element<T> operator=(const Element<T>& e) ;
} ;


template <class E>
class List
{
private:
	Element<E> *first ;
	Element<E> *last ;
	Element<E> *current ;
public:
	List() ;
	List(const List<E>& l)  ;
	~List();
	List<E> operator=(const List<E>& l) ;
	List<E>& operator+(const List<E>& l) ;

    friend std::ostream& operator<<(std::ostream &out, const List<E>& l)
    {
        if(l.first)
        {
            Element<E> *cursor = l.first ;
            do
            {
                out << cursor->data << "->" ;
                cursor = cursor->next ;
            }
            while(cursor) ;
        }

        return out ;
    }

    friend std::istream& operator>>(std::istream &in, const List<E>& l)
    {
        if(l.first)
        {
            Element<E> *cursor = l.first ;
            do
            {
                in >> cursor->data  ;
                cursor = cursor->next ;
            }
            while(cursor) ;
        }

        return in ;
    }

	void vider() ;
} ;

#endif // LIST_HPP_INCLUDED
