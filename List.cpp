#include "List.hpp"

// Les éléments
template <class T>
Element<T>::Element() {}

template <class T>
Element<T>::~Element()
{
	if(this->next) {
		delete next ;
	}
}

template <class T>
Element<T>::Element(const Element<T> &e)
{
	this->date = e.data ;
	this->next = e.next ;
}

template <class T>
Element<T> Element<T>::operator=(const Element<T>& e)
{
	this->data = e.data ;
	this->next = e.next ;

	return *this ;
}

// La liste

template <class E>
List<E>::List() {}

template <class E>
List<E>::List(const List<E>& l)
{
	this->current = l.current ;
	this->last = l.last ;
	this->current = l.current ;
}

template <class E>
List<E>::~List()
{
	if(this->first)
	{
		Element<E> *cursor = nullptr ;

		do
		{
				cursor = this->first ;
				this->first = this->first->next ;
				delete cursor ;
		}
		while(cursor) ;
	}
}

template <class E>
List<E> List<E>::operator=(const List<E>& l)
{
	this->current = l.current ;
	this->last = l.last ;
	this->current = l.current ;

	return *this;
}

template <class E>
List<E>& List<E>::operator+(const List<E>& l)
{
	this->last->next = l.first ;

	return *this ;
}

template <class E>
void List<E>::vider()
{
	if(this->first)
	{
		Element<E> *cursor = nullptr ;

		do
		{
				cursor = this->first ;
				this->first = this->first->next ;
				delete cursor ;
		}
		while(cursor) ;
	}
}
