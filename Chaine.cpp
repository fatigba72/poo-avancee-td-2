#include "Chaine.hpp"

Chaine::Chaine()
{
	this->capacity = CAPACITE_DEFAUT ;
	this->strlen = 0 ;
	this->str = new char[CAPACITE_DEFAUT] ;
	if(!this->str) {
		throw std::string("impossible de créer la chaine") ;
	}
}

Chaine::Chaine(const Chaine &c)
{
	this->capacity = c.capacity ;
	this->strlen = c.strlen ;
	if(c.str)
	{
		this->str = (char*) malloc(this->capacity*sizeof(char)) ;
		if(!this->str) {
				throw std::string("impossible de créer la chaine") ;
		}
		memcpy(this->str, &c.str, this->capacity*sizeof(char)) ;
	}
}

Chaine::~Chaine()
{
  if(this->str) {
		free(this->str) ;
  }
}

Chaine& Chaine::operator=(const Chaine& c)
{
	this->capacity = c.capacity ;
	this->strlen = c.strlen ;
	free(this->str) ;

	if(c.str)
	{
		this->str = (char*) malloc(this->capacity*sizeof(char)) ;
		if(!this->str) {
				throw std::string("impossible de copier la chaine c vers this") ;
		}
		memcpy(this->str, &c.str, this->capacity*sizeof(char)) ;
	}

	return *this ;
}

void Chaine::setStr(const char* c_string)
{
  if(c_string)
	{
		size_t len = std::strlen(c_string) ;
		if(len == 0) {
			return ;
		}

		this->strlen = (int) len ;

		if(len > this->capacity)
		{
			int sup = (len % this->capacity) ? 1 : 0 ;
			this->capacity = len / this->capacity + sup ;
			this->str = (char*) realloc(this->str, this->capacity*sizeof(char)) ;
			memcpy(this->str, c_string, this->capacity*sizeof(char)) ;
		}
		else
		{
			for(int i = 0; i< this->strlen; i++) {
				this->str[i] = c_string[i] ;
			}

			this->str[len + 1] = 0 ;
		}
	}

	return ;
}
